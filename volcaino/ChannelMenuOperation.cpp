#include "ChannelMenuOperation.h"
#include "DataBase.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QLineEdit>
#include <QLabel>
#include <QComboBox>
#include <QRadioButton>
#include <QGroupBox>
#include <QPushButton>
#include <QTableWidgetItem>
#include <QMessageBox>

ChannelMenuOperation::ChannelMenuOperation()
{
}
ChannelMenuOperation::~ChannelMenuOperation()
{

}
ChannelMenuOperation* ChannelMenuOperation::getInstance(){

    static ChannelMenuOperation theInstance;
    return &theInstance;

}
void ChannelMenuOperation::listChan(QWidget  *widget)
{

    TableViewOperation * table = qobject_cast<TableViewOperation *>(widget);
    DEBUG_TRACE(table->getChannelList());

    QDialog *dialog=new QDialog();
    dialog->setWindowFlags(dialog->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    dialog->setWindowTitle(tr("DISPLAY CHANNELS"));
    dialog->setMinimumSize(220,100);

    QHBoxLayout *hLayout = new QHBoxLayout(dialog);
    QGroupBox *window = new QGroupBox(dialog);
    hLayout->addWidget(window);

    window->setTitle(tr("Hidden Channels"));

    QFormLayout *layout = new QFormLayout(window);
    auto list=table->getHiddenChannelList();
    if(list.size()>0){
        for(int item:list){
            QLabel *name = new QLabel(tr(table->horizontalHeaderItem(item)->text().toUtf8()));
            QRadioButton *status=new QRadioButton(dialog);
            layout->addRow(name, status);
            connect(status,&QRadioButton::clicked,dialog,[table,item,name,status](){
                if(status->isChecked()){
                    table->setChannelToDisplayState(item);
                    name->setEnabled(false);
                    status->setEnabled(false);
                }
            });
        }
        QPushButton *cancel=new QPushButton(tr("CLOSE"));
        layout->addRow(cancel);
        window->show();
        dialog->showNormal();

        connect(cancel, &QPushButton::clicked, dialog,[dialog](){dialog->close();});
    }
    else{
        QMessageBox msgBox;
        msgBox.setWindowFlags(dialog->windowFlags() & ~Qt::WindowContextHelpButtonHint| Qt::FramelessWindowHint);
        msgBox.setText("None of the Channels is hidden. ");
        msgBox.exec();
    }
}

void ChannelMenuOperation::newChan(QWidget  *widget)
{
    TableViewOperation * table = qobject_cast<TableViewOperation *>(widget);


    DEBUG_TRACE(table->getSelectedChannel());
    DEBUG_TRACE(table->getTableName());
    DEBUG_TRACE(table->getChannelList());
    DEBUG_TRACE(table->horizontalHeaderItem(table->getSelectedChannel())->text());

    QStringList channels=table->getChannelList();

    QDialog *dialog=new QDialog();
    dialog->setWindowFlags(dialog->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    dialog->setWindowTitle(tr("NEW CHANNEL"));

    QGroupBox *window = new QGroupBox(dialog);
    window->setTitle(tr("Channel Details"));
    QLabel *name = new QLabel(tr("Name"));
    QLineEdit *nameLineEdit = new QLineEdit();
    QLabel *type = new QLabel(tr("Type"));
    QComboBox *typeComboBox = new QComboBox;
    typeComboBox->addItem(tr("INTEGER"));
    typeComboBox->addItem(tr("TEXT"));
    typeComboBox->addItem(tr("DOUBLE"));
    typeComboBox->addItem(tr("FLOAT"));
    QLabel *defaultVal = new QLabel(tr("Default"));
    QLineEdit *defLineEdit = new QLineEdit();
    defLineEdit->setText("0");
    QLabel *notNull = new QLabel(tr("Not Null"));
    QRadioButton *notNullCheckBox= new QRadioButton;
    notNullCheckBox->setChecked(true);

    QFormLayout *layout = new QFormLayout(window);
    layout->addRow(name, nameLineEdit);
    layout->addRow(type, typeComboBox);
    layout->addRow(defaultVal, defLineEdit);
    layout->addRow(notNull,notNullCheckBox);

    QPushButton *ok=new QPushButton(tr("OK"));
    QPushButton *cancel=new QPushButton(tr("CANCEL"));
    layout->addRow(ok,cancel);
    window->show();
    dialog->showNormal();

    connect(typeComboBox,&QComboBox::currentTextChanged,dialog,[typeComboBox,defLineEdit](){
        switch (typeComboBox->currentIndex())
        {
        case 0:
            defLineEdit->setText("0");
            break;

        case 1:
            defLineEdit->setText("N/A");
            break;
        case 2:
            defLineEdit->setText("0.00");
            break;

        case 3:
            defLineEdit->setText("0.0");
            break;

        default:

            break;
        }
    });


    connect(nameLineEdit,&QLineEdit::textChanged,dialog,[ok,channels,nameLineEdit](){
        if(channels.contains(nameLineEdit->text(),Qt::CaseInsensitive)){
            nameLineEdit->setStyleSheet("QLineEdit { background-color: red }");
            ok->setDisabled(true);
        }
        else{
            nameLineEdit->setStyleSheet("QLineEdit { background-color: white }");
            ok->setDisabled(false);
        }

    });
    auto Db=DataBase::getInstance();

    connect(ok, &QPushButton::clicked, dialog,[Db,table,dialog,nameLineEdit,typeComboBox,defLineEdit,notNullCheckBox](){
        Db->addColumn(table->getTableName(),nameLineEdit->text().toLower(),typeComboBox->currentText(),notNullCheckBox->isChecked(),defLineEdit->text());
        dialog->close();
        table->refreshTable();

    });
    connect(cancel, &QPushButton::clicked, dialog,[dialog](){dialog->close();});

}
void ChannelMenuOperation::deleteChan(QWidget  *widget)
{
    TableViewOperation * table = qobject_cast<TableViewOperation *>(widget);
    QString column=table->horizontalHeaderItem(table->getSelectedChannel())->text();
    QString str="Do you want to delete channel "+column+" ?";
    QMessageBox msgBox;
    msgBox.setWindowTitle("DELETE CHANNEL");
    msgBox.setText(str);
    msgBox.setStandardButtons(QMessageBox::Ok| QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Cancel);
    int ret = msgBox.exec();
    auto Db=DataBase::getInstance();

    switch (ret) {
    case QMessageBox::Ok:
        Db->deleteColumn(table->getTableName(),column.toLower());
        msgBox.close();
        table->setChannelToDisplayState(table->getSelectedChannel());
        table->refreshTable();
        break;
    case QMessageBox::Cancel:
        msgBox.close();
        break;
    default:
        // should never be reached
        break;
    }

}
void ChannelMenuOperation::copyCreate(QWidget  *widget)
{
    TableViewOperation * table = qobject_cast<TableViewOperation *>(widget);


    DEBUG_TRACE(table->getSelectedChannel());
    DEBUG_TRACE(table->getTableName());
    DEBUG_TRACE(table->getChannelList());
    DEBUG_TRACE(table->horizontalHeaderItem(table->getSelectedChannel())->text());

    QStringList channels=table->getChannelList();

    QDialog *dialog=new QDialog();
    dialog->setWindowFlags(dialog->windowFlags() & ~Qt::WindowContextHelpButtonHint);
    dialog->setWindowTitle(tr("COPY CHANNEL"));

    QGroupBox *window = new QGroupBox(dialog);
    window->setTitle(tr("Channel Details"));
    QLabel *name = new QLabel(tr("Name"));
    QLineEdit *nameLineEdit = new QLineEdit();
    QLabel *type = new QLabel(tr("Type"));
    QComboBox *typeComboBox = new QComboBox;
    typeComboBox->addItem(tr("INTEGER"));
    typeComboBox->addItem(tr("TEXT"));
    typeComboBox->addItem(tr("DOUBLE"));
    typeComboBox->addItem(tr("FLOAT"));
    QLabel *defaultVal = new QLabel(tr("Default"));
    QLineEdit *defLineEdit = new QLineEdit();
    defLineEdit->setText("0");

    QLabel *notNull = new QLabel(tr("Not Null"));
    QRadioButton *notNullCheckBox= new QRadioButton;
    notNullCheckBox->setChecked(true);
    QLabel *chanList = new QLabel(tr("Channel List"));
    QComboBox *chanComboBox = new QComboBox;
    auto list=table->getChannelList();
    for(QString item:list){
        chanComboBox->addItem(item);
    }
    chanComboBox->setCurrentText(table->horizontalHeaderItem(table->getSelectedChannel())->text());
    QFormLayout *layout = new QFormLayout(window);
    layout->addRow(name, nameLineEdit);
    layout->addRow(type, typeComboBox);
    layout->addRow(defaultVal, defLineEdit);
    layout->addRow(notNull,notNullCheckBox);
    layout->addRow(chanList,chanComboBox);

    QPushButton *ok=new QPushButton(tr("OK"));
    QPushButton *cancel=new QPushButton(tr("CANCEL"));
    layout->addRow(ok,cancel);
    window->show();
    dialog->showNormal();

    connect(typeComboBox,&QComboBox::currentTextChanged,dialog,[typeComboBox,defLineEdit](){
        switch (typeComboBox->currentIndex())
        {
        case 0:
            defLineEdit->setText("0");
            break;

        case 1:
            defLineEdit->setText("N/A");
            break;
        case 2:
            defLineEdit->setText("0.00");
            break;

        case 3:
            defLineEdit->setText("0.0");
            break;

        default:
            break;
        }
    });


    connect(nameLineEdit,&QLineEdit::textChanged,dialog,[ok,channels,nameLineEdit](){
        if(channels.contains(nameLineEdit->text(),Qt::CaseInsensitive)){
            nameLineEdit->setStyleSheet("QLineEdit { background-color: red }");
            ok->setDisabled(true);
        }
        else{
            nameLineEdit->setStyleSheet("QLineEdit { background-color: white }");
            ok->setDisabled(false);
        }

    });
    auto Db=DataBase::getInstance();

    connect(ok, &QPushButton::clicked, dialog,[Db,table,dialog,nameLineEdit,typeComboBox,defLineEdit,notNullCheckBox,chanComboBox](){
        Db->copyColumn(table->getTableName(),nameLineEdit->text().toLower(),typeComboBox->currentText(),notNullCheckBox->isChecked(),defLineEdit->text(),chanComboBox->currentText());
        dialog->close();
        table->refreshTable();

    });
    connect(cancel, &QPushButton::clicked, dialog,[dialog](){dialog->close();});

}

void ChannelMenuOperation::hide(QWidget  *widget)
{
    TableViewOperation * table = qobject_cast<TableViewOperation *>(widget);

    DEBUG_TRACE(table->getSelectedChannel());
    DEBUG_TRACE(table->getTableName());
    DEBUG_TRACE(table->getChannelList());
    DEBUG_TRACE(table->horizontalHeaderItem(table->getSelectedChannel())->text());
    table->setChannelToHiddenState(table->getSelectedChannel());

}


void ChannelMenuOperation::displayAll(QWidget  *widget)
{
    TableViewOperation * table = qobject_cast<TableViewOperation *>(widget);
    DEBUG_TRACE(table->getChannelList());
    auto list=table->getChannelList();
    for(int i=0;i<list.size();++i)
        table->setChannelToDisplayState(i);
    table->refreshTable();

}


