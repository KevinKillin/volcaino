#ifndef CHANNELMENUOPERATION_H
#define CHANNELMENUOPERATION_H

#include <QObject>
#include <QMenu>
#include <QTableWidget>
#include <QDialog>
#include "TypeDefConst.h"
#include "TableViewOperation.h"

class TableViewOperation;

class ChannelMenuOperation:public QObject
{
    Q_OBJECT
public:

    ///default constructor
    ChannelMenuOperation();

    /// Class destructor
    ~ChannelMenuOperation();

    /// @brief static function
    ///
    /// @return   Instance of class TableViewOperation
    ///
    static ChannelMenuOperation* getInstance();


public slots:
    /// @brief This function display all the available channels that are not currently
    /// being displayed in the table window. This function also gives an option to unhide
    /// the channel.
    /// @param widget: A widget of type TableViewOperation to match the signal
    /// @return  void
    ///
    void listChan(QWidget  *widget);

    /// @brief This function creates a new channel in the database.
    /// @param widget: A widget of type TableViewOperation to match the signal
    /// @return  void
    ///
    void newChan(QWidget *table);

    /// @brief This function deletes a selected channel from database.
    /// @param widget: A widget of type TableViewOperation to match the signal
    /// @return  void
    ///
    void deleteChan(QWidget  *widget);

    /// @brief This function copies the data of selected channel and creates a
    /// new channel with the copied data.
    /// @param widget: A widget of type TableViewOperation to match the signal
    /// @return  void
    ///
    void copyCreate(QWidget  *widget);

    /// @brief This function hides the selected channel.
    /// @param widget: A widget of type TableViewOperation to match the signal
    /// @return  void
    ///
    void hide(QWidget  *widget);

    /// @brief This function displays all the hidden channels.
    /// @param widget: A widget of type TableViewOperation to match the signal
    /// @return  void
    ///
    void displayAll(QWidget  *widget);


};

#endif // CHANNELMENUOPERATION_H
