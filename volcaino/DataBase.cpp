#include "DataBase.h"
#include "mainwindow.h"
#include <QSqlDriver>
#include <QDebug>
#include <QSqlError>
#include <QDir>
#include <QThread>

#define cDataBase "/DATABASE/VOLCAINO.db"

#define ERROR_FAILED_QUERY(qsql) qDebug() << __FILE__ << __LINE__ << "FAILED QUERY [" \
    << (qsql).lastQuery() << "]" << (qsql).lastError()

DataBase::DataBase():mpDb(nullptr),mSql(nullptr)
{

}

DataBase::~DataBase()
{
    this->closeDatabase();
}

DataBase* DataBase::getInstance(){

    static DataBase theInstance;
    return &theInstance;

}
void DataBase::createDatabase(const QString str){
    mpDb = new QSqlDatabase(QSqlDatabase::addDatabase("QSQLITE", str));
    mpDb->setHostName("localhost");
    mpDb->setDatabaseName(this->setDatabase());

    DEBUG_TRACE(mpDb->open());

    mSql=QSqlQuery("",QSqlDatabase::database("MainThreadConnection"));
}

QString DataBase::setDatabase(){

    QDir dataBaseDir;
    dataBaseDir.setPath(QDir::currentPath());
    dataBaseDir.cdUp();
    QString dbFile=dataBaseDir.path()+cDataBase;

    DEBUG_TRACE(dbFile);

    return dbFile;
}

bool DataBase::openDatabase(){

    if (!mpDb->isOpen())
    {
        mpDb->open();
    }

    return mpDb->isOpen();
}


void DataBase::closeDatabase(){

    if(mpDb->isOpen()){
        mpDb->close();
    }
}

void DataBase::startTransaction(){

    QString transaction = "BEGIN TRANSACTION;";
    DEBUG_TRACE(transaction);
    if(!mSql.exec(transaction))
        ERROR_FAILED_QUERY(mSql);
}

void DataBase::closeTransaction(){

    QString transaction = "COMMIT;";
    DEBUG_TRACE(transaction);
    if(!mSql.exec(transaction))
        ERROR_FAILED_QUERY(mSql);
}

void DataBase::insertRecord(const QString& record, const QString& table){

    QString query="INSERT INTO "+table+" VALUES("+record+");";
    if(!mSql.exec(query))
        ERROR_FAILED_QUERY(mSql);
}

void DataBase::createTable(const QString &tableName, const QStringList &columnList){

    QString stmt = "CREATE TABLE "+tableName+"(";
    for(int i=0;i<columnList.size();++i)
        stmt.append(columnList[i]);

    stmt.chop(1);
    stmt.append(");");

    DEBUG_TRACE(stmt);

    if(!mSql.exec(stmt))
        ERROR_FAILED_QUERY(mSql);
}

QStringList DataBase::getTableList(){

    QStringList list;
    QString query="SELECT name FROM sqlite_master WHERE type='table' order by name;";

    DEBUG_TRACE(query);

    if(!mSql.exec(query)){

        ERROR_FAILED_QUERY(mSql);
    }
    else{
        while (mSql.next()) {
            list.append(mSql.value(0).toString());
        }
    }
    return list;
}

void DataBase::addColumn(const QString &table,const QString &column,const QString &type,const bool &nullFlag,const QString defValue){

    QString nullNotNull="NOT NULL";
    if(!nullFlag)nullNotNull="NULL";

    QString query="ALTER TABLE "+table+" ADD COLUMN "+column+" "+type+ " DEFAULT "+defValue+" "+nullNotNull;
    DEBUG_TRACE(query);

    if(!mSql.exec(query))
        ERROR_FAILED_QUERY(mSql);
}

void DataBase::deleteColumn(const QString &table,const QString &column){

    QString query="ALTER TABLE "+table+" DROP COLUMN "+column;
    DEBUG_TRACE(query);

    if(!mSql.exec(query))
        ERROR_FAILED_QUERY(mSql);
}

void DataBase::copyColumn(const QString &table,const QString &column,const QString &type,const bool &nullFlag,const QString defValue, const QString columnToCopy){

    this->addColumn(table,column,type,nullFlag,defValue);

    QString query="UPDATE "+table+ " SET "+column+" = "+columnToCopy;
    DEBUG_TRACE(query);

    if(!mSql.exec(query))
        ERROR_FAILED_QUERY(mSql);
}
