/**
 *  @file   DataBase.h
 *  @brief  SQLite Database functionality
 *  @author Rachana Kapoor
 *  @date   2021-12-14
 ***********************************************/

#ifndef DATABASE_H
#define DATABASE_H

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QObject>
#include "TypeDefConst.h"
class DataBase:public QObject
{
    Q_OBJECT
public:

    ///default constructor
    DataBase();

    /// Class destructor
    ~DataBase();

    /// @brief static function
    ///
    /// @return   Instance of class Database
    ///
    static DataBase* getInstance();

    /// @brief This function is responsible for opening the database if not opened
    ///
    /// @return   True is Database is open else False
    ///
    bool openDatabase();

    /// @brief This function is responsible for adding the database on
    /// startup. This function also sets the database file path.
    /// @param str: database connection name
    /// @return  void
    ///
    void createDatabase(const QString);

    /// @brief This function is responsible for closing the database when not in use.
    /// @return void
    ///
    void closeDatabase();


    /// @brief This function is responsible for initiating the database transaction.
    /// @return void
    ///
    void startTransaction();

    /// @brief This function is responsible for saving the changes made to the database.
    /// @return void
    ///
    void closeTransaction();

    /// @brief This function inserts the valid data record in the database.
    /// @param record: a record of line data
    /// @param table: table name to store record
    /// @return void
    ///
    void insertRecord(const QString& record, const QString& table);

    /// @brief This function creates a table schema in the database.
    /// @param tableName: Name of the table
    /// @param columnList: List of the channels/columns
    /// @return void
    ///
    void createTable(const QString &tableName, const QStringList &columnList);

    /// @brief This function retrieves the list of tables stored in the database.
    /// @return List of the tables createdin the database.
    ///
    QStringList getTableList();

    /// @brief This function adds new column in the table.
    /// @param table Name: Name of the table
    /// @param column: Name of the column
    /// @param type: Type of the column
    /// @param null flag: Flag indicates whether a null value or not
    /// @param default value: default value of the newly created column
    /// @return  void
    ///
    void addColumn(const QString &tableName,const QString &column,const QString &type,const bool &nullFlag,const QString defValue);

    /// @brief This function deletes the column from table.
    /// @param table Name: Name of the table
    /// @param column: Name of the column
    /// @return  void
    ///
    void deleteColumn(const QString &table,const QString &column);

    /// @brief This function adds new column in the table.
    /// @param table Name: Name of the table
    /// @param column: Name of the column
    /// @param type: Type of the column
    /// @param null flag: Flag indicates whether a null value or not
    /// @param default value: default value of the newly created column
    /// @param column to copy: Name of the column needs to be copied
    /// @return  void
    ///
    void copyColumn(const QString &table,const QString &column,const QString &type,const bool &nullFlag,const QString defValue, const QString columnToCopy);


private:
    /// @brief This function sets the path of the database file.
    /// @return path of the database file.
    ///
    QString setDatabase();

    QSqlDatabase *mpDb;

    QSqlQuery mSql;

};

#endif // DATABASE_H
