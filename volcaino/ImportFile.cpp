#include "ImportFile.h"
#include "DatabaseTable.h"
#include "mainwindow.h"
#include <QThread>
#include <QSqlQuery>
#include <QFileDialog>
#include <fstream>
#include <regex>

ImportFile::ImportFile():mTable{"N/A"}
{

    QThread *thread=new QThread;
    this->moveToThread(thread);
    thread->start();
    connect(thread,&QThread::finished,this,&QObject::deleteLater);

}
ImportFile* ImportFile::getInstance(){
    static ImportFile theInstance;
    return &theInstance;
}

void ImportFile::setTableName(QString table){
    if(table!="")
        if (table!=mTable)
        {
            mTable=table;
        }
}

void ImportFile::setFile(QString file){

    DEBUG_TRACE(file);

    if(file.endsWith(".csv")){
        this->setAndLoadCSVFile(file);
    }
    else if(file.endsWith(".XYZ")){
        this->setAndLoadXYZFile(file);
    }
}

QString& ImportFile::getTableName(){

    return mTable;
}

void ImportFile::setAndLoadCSVFile(QString filename){

    if( !filename.isNull() )
    {
        DEBUG_TRACE("selected file : " + filename.toUtf8());

        emit started();

        auto db=DataBase::getInstance();
        auto status=db->openDatabase();


        if(status){

            QFile file(filename);
            if(file.open (QIODevice::ReadOnly)){

                QTextStream ts (&file);

                QStringList header = ts.readLine().split(',');
                QStringList line = ts.readLine().split(',');
                auto typeList=this->typeDeduction(header,line);

                DEBUG_TRACE(typeList);

                db->createTable(this->getTableName(),typeList);
                ts.seek(0);
                ts.readLine();

                db->startTransaction();

                while(!ts.atEnd()){

                    std::vector<QString> values;

                    QStringList line = ts.readLine().split(',');
                    QString record="'"+line.at(0)+"',";
                    QString type1=line.at(0);
                    for(int i=1; i<line .length ();++i){

                        record.append(line.at(i));
                        record.append(",");
                    }
                    record.chop(1);
                    db->insertRecord(record,this->getTableName()); //Extend functionality to cater different data tables
                }

                db->closeTransaction();
            }

            file.close ();

        }
        emit finished();
    }
}

void ImportFile::setAndLoadXYZFile(QString filename){

    bool reading=false;

    if( !filename.isNull() )
    {
        DEBUG_TRACE("selected file : " + filename.toUtf8());

        emit started();

        auto db=DataBase::getInstance();

        auto status=db->openDatabase();

        if(status){
            QFile file(filename);
            if(file.open (QIODevice::ReadOnly)){
                QTextStream stream(&file);
                QString line;
                QString lineNo;
                QString lineType;
                QString flightNo;
                QString date;
                QStringList headerList;
                QStringList dataList;
                int counter=0;

                for(int counter=1;counter<13;++counter){
                    line = stream.readLine();
                    if(counter==6){
                        headerList.clear();
                        headerList.append("flightNo");
                        headerList.append("flightDate");
                        headerList.append("lineType");
                        headerList.append("lineNo");
                        headerList.append(line.simplified().split(" "));
                        headerList.removeOne("/");
                        headerList.removeOne("");

                        DEBUG_TRACE(headerList);
                    }
                    if(line.contains("//Flight")){
                        flightNo.clear();
                        flightNo.append(line.simplified().split(" ").last());

                        DEBUG_TRACE(line+ " " + flightNo);

                        continue;
                    }
                    if(line.contains("//Date")){
                        date.clear();
                        date.append(line.simplified().split(" ").last());

                        DEBUG_TRACE(date);

                        continue;
                    }
                    if(line.contains("Line")||line.contains("Tie"))
                    {
                        lineNo.clear();
                        lineNo.append(line.simplified().split(" ").last());
                        lineType.clear();
                        lineType.append(line.simplified().split(" ").first());
                    }
                    if(counter==12){
                        dataList.clear();
                        dataList.append(flightNo);
                        dataList.append(date);
                        dataList.append(lineType);
                        dataList.append(lineNo);
                        dataList.append(line.simplified().split(" "));
                        auto typeList=this->typeDeduction(headerList,dataList);

                        DEBUG_TRACE(typeList);

                        db->createTable(this->getTableName(),typeList);
                    }

                }
                db->startTransaction();
                stream.seek(0);
                do
                {
                    counter=counter+1;

                    line = stream.readLine();

                    if(line.contains("//Flight")){
                        flightNo.clear();
                        flightNo.append(line.simplified().split(" ").last());
                        DEBUG_TRACE(line+" "+flightNo);
                        continue;
                    }
                    if(line.contains("//Date")){
                        date.clear();
                        date.append(line.simplified().split(" ").last());
                        DEBUG_TRACE(date);
                        reading=true;
                        continue;
                    }
                    if(reading){
                        if(line.contains("Line")||line.contains("Tie"))
                        {
                            lineNo.clear();
                            lineNo.append(line.simplified().split(" ").last());
                            lineType.clear();
                            lineType.append(line.simplified().split(" ").first());
                        }
                        else{

                            QStringList lineStr=line.simplified().split(" ");
                            QString record=flightNo+",'"+date+"','"+lineType+"',"+lineNo+",";
                            for(int i=0; i<lineStr .length ();++i){
                                record.append("'");
                                record.append(lineStr.at(i));
                                record.append("',");
                            }
                            record.chop(1);
                            if(counter==12)DEBUG_TRACE(record);
                            db->insertRecord(record,this->getTableName());
                        }
                    }
                } while (!line.isNull());
                db->closeTransaction();
            }
            file.close();
        }
        emit finished();
    }

}

QStringList ImportFile::typeDeduction(QStringList &theHeader,QStringList &theData){

    QStringList columnList;

    DEBUG_TRACE(theData);
    DEBUG_TRACE(theHeader);

    for (int i=0;i<theData.size();++i){
        if(isString(theData[i])){
            columnList<<theHeader[i]+" TEXT,";
            continue;
        }
        if(isNumber(theData[i])){
            columnList<<theHeader[i]+" INTEGER,";
            continue;
        }
        if(isDecimal(theData[i])){
            columnList<<theHeader[i]+" FLOAT,";
            continue;
        }

        if(isDate(theData[i])){
            columnList<<theHeader[i]+" TEXT,";
            continue;
        }
        else{
            columnList<<theHeader[i]+" TEXT,";
        }

    }
    return columnList;

}

bool ImportFile::isDecimal(QString& value){

    std::regex e ("^-?[0-9]+\.(0?[0-9])*$");      //^\d+\.(0?[0-9]|1[0-3])$

    if (std::regex_match(value.toStdString(),e))
        return true;

    return false;
}

bool ImportFile::isString(QString& value){

    std::regex e ("([A-Za-z])+");

    if (std::regex_search (value.toStdString(),e))
        return true;

    return false;
}
bool ImportFile::isNumber(QString& value){

    std::regex e ("^0$|^-?[1-9][0-9]*$");

    if (std::regex_match (value.toStdString(),e))
        return true;

    return false;
}

bool ImportFile::isDate(QString& value){

    std::regex e ("[0-9]{1,4}[-\/. ][0-9]{1,2}[-\/. ][0-9]{1,4}");

    if (std::regex_match (value.toStdString(),e))
        return true;

    return false;
}
