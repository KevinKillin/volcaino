#include "TableViewOperation.h"
#include <QSqlDatabase>
#include <QMdiSubWindow>
#include <QScrollBar>
#include <QHeaderView>
#include <QInputDialog>
#include <QAbstractItemModel>
#include <QVBoxLayout>
#include "mainwindow.h"
#include <QMenu>
#include <QSignalMapper>

TableViewOperation::TableViewOperation():mRow(1000),mColumn(0),mCount(0),mMin(1),mMax(10000),
    mNoOfPages(0),mRemRecord(0),mPageCount(0),mNoOfRows(0),mTable("N/A"),mFilterColumn(""),
    mFilterFlag(false),mSelectedChan(-1),mHiddenChannels{},mClick(false)
{
    mSql=QSqlQuery("",QSqlDatabase::database("MainThreadConnection"));
    mpChannel=ChannelMenuOperation::getInstance();
    this->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this->horizontalHeader(), &QHeaderView::customContextMenuRequested, this, &TableViewOperation::setChanContextMenu);
}

TableViewOperation::~TableViewOperation()
{
    this->clear();
}

TableViewOperation* TableViewOperation::getInstance(){

    static TableViewOperation theInstance;
    return &theInstance;

}


void TableViewOperation::setMDIArea(QMdiArea *mdiArea){
    mMdiArea=mdiArea;
}

void TableViewOperation::setTableName(QString table){

    mTable=table;
    this->setWindowTitle(mTable.toUpper());

}

void TableViewOperation::renameChannel(int channel){
    mClick=true;
    QString oldHeader=this->horizontalHeaderItem(channel)->text();
    bool ok;

    QString text = QInputDialog::getText(this,tr("Enter Channel Name"),
                                         tr("Channel:"), QLineEdit::Normal,
                                         oldHeader, &ok,(this->windowFlags() & ~Qt::WindowContextHelpButtonHint| Qt::FramelessWindowHint));
    if (ok && !text.isEmpty())
        this->horizontalHeaderItem(channel)->setText(text);
    QString query="";
    query="ALTER TABLE "+mTable+" RENAME COLUMN "+oldHeader +" TO "+ text;
    if(mSql.exec(query)){
        DEBUG_TRACE(query);
        this->setColumn();
    }

}

void TableViewOperation::filterChannel(int channel){

    QString header=this->horizontalHeaderItem(channel)->text();
    mFilterColumn.clear();
    if((header.contains("FLIGHT")||header.contains("LINE"))&&!(header.contains("TYPE"))&&!(header.contains("DATE"))){
        mFilterFlag=true;
        mFilterColumn=header;
        QString query="";
        query="SELECT distinct "+header+" FROM "+mTable+" order by "+header+";";
        DEBUG_TRACE(query);
        QStringList list;
        if(mSql.exec(query)){
            while(mSql.next()){
                list.append(mSql.value(0).toString());
            }
        }
        QDialog *dialog=new QDialog(this);
        dialog->setWindowTitle("FILTER DATA");
        dialog->setMaximumHeight(150);
        dialog->setMaximumWidth(200);
        dialog->setWindowFlags(dialog->windowFlags() & ~Qt::WindowContextHelpButtonHint);
        QVBoxLayout* layout = new QVBoxLayout;
        dialog->setLayout(layout);

        QListWidget * widget = new QListWidget;

        QLabel *lineLabel = new QLabel(dialog);

        layout->addWidget(lineLabel);
        layout->addWidget(widget);
        for(int i = 0; i != list.size(); ++i)
            (new QListWidgetItem(widget))->setText(list[i]);

        lineLabel->setText(header);
        if(list.size()==1){
            widget->setEnabled(false);
            mFilterFlag=false;
            this->showData();
        }
        dialog->show();
        connect (widget, &QListWidget::itemClicked, this, &TableViewOperation::setFilter) ;
    }

}
void TableViewOperation::setFilter(QListWidgetItem *item){


    QString query="";
    query="SELECT count(*) from "+mTable+" where "+ mFilterColumn+"='"+item->text()+"';";
    if(mSql.exec(query)){
        mSql.first();
        mCount=mSql.value(0).toInt();
    }

    query="";
    query="SELECT ROWID,* from "+mTable+" where "+ mFilterColumn+"='"+item->text()+"';";
    DEBUG_TRACE(query);
    if(mSql.exec(query)){
        this->setRowCount(0);
        this->setRowCount(mCount);
        DEBUG_TRACE(mCount);
        this->setHorizontalHeaderLabels(mColumnNames);
        this->setColumnCount(mColumn);

        for(unsigned int index=0;index<mCount;++index)
        {
            mSql.next();
            for (unsigned int i=0;i<mColumn;i++){
                this->setItem(index,i,new QTableWidgetItem(mSql.value(i).toString()));
            }
        }
    }


}
void TableViewOperation::setSliderData(){

    QString query="";
    query="SELECT ROWID,* FROM "+mTable+" WHERE ROWID BETWEEN "+QString::number(mMin)+" AND "+QString::number(mMax)+";";

    this->clear();

    this->setHorizontalHeaderLabels(mColumnNames);

    if(!mSql.exec(query)){

        DEBUG_TRACE(query);
    }
    else{
        DEBUG_TRACE(mSql.size());

        this->setRowCount(mRow);
        this->setColumnCount(mColumn);
        for(unsigned int index=0;index<mRow;++index)
        {
            mSql.next();
            for (unsigned int i=0;i<mColumn;i++){
                this->setItem(index,i,new QTableWidgetItem(mSql.value(i).toString()));
            }
        }
    }

}

void TableViewOperation::setRow(){

    if(mSql.exec("SELECT count(*) FROM "+mTable+";")){
        mSql.first();
        mCount=mSql.value(0).toInt();
    }
    if(mRow>mCount){
        mRow=mCount;
        mMax=mCount;
    }
    DEBUG_TRACE(mCount);

}
void TableViewOperation::setColumn(){

    mColumnNames.clear();
    mColumnNames.append("ROWID");
    QString query="SELECT * FROM PRAGMA_TABLE_INFO('"+mTable+"');";
    if(mSql.exec(query)){
        while (mSql.next()) {
            mColumnNames.append(mSql.value(1).toString().toUpper());
        }
    }
    DEBUG_TRACE(mColumnNames);
    mColumn=mColumnNames.size();

}
void TableViewOperation::setTableRange(int i){
    if(this->item(i,0)->text().toInt()>900){
        mMin=this->item(i,0)->text().toInt()-900;
    }
    else{
        mMin=0;
    }
    mMax=mMin+1000;
    if(mMax>=mCount)mMax=mCount;

    this->setSliderData();
}


void TableViewOperation::itemClicked(QListWidgetItem *item){

    this->setTableName(item->text());
    this->setColumn();
    this->setRow();
    this->showData();
}

void TableViewOperation::showData(){

    DEBUG_TRACE(mMax);
    if(mMax>0){
        QString query="";
        query="SELECT ROWID,* FROM "+mTable+";";

        if(!mSql.exec(query)){

            DEBUG_TRACE(query);
        }
        else{
            DEBUG_TRACE(query);
            DEBUG_TRACE(mSql.size());

            this->setRowCount(mCount);
            this->setColumnCount(mColumn);

            mNoOfPages=mCount/10000;
            mRemRecord=mCount%10000;
            DEBUG_TRACE(mNoOfPages);
            DEBUG_TRACE(mRemRecord);

            setRowsToFetch(0);

        }

        this->setHorizontalHeaderLabels(mColumnNames);

        if(mHiddenChannels.size()>0)
            for(int item:mHiddenChannels){
                if(mColumnNames.contains(this->horizontalHeaderItem(item)->text(),Qt::CaseInsensitive))
                    this->setColumnHidden(item,true);
            }
        this->show();


        connect(this->verticalScrollBar(),&QScrollBar::valueChanged,this,&TableViewOperation::setRowsToFetch);
        connect(this->horizontalHeader(),&QHeaderView::sectionDoubleClicked,this,&TableViewOperation::renameChannel);
        connect(this->horizontalHeader(),&QHeaderView::sectionPressed,this,&TableViewOperation::filterChannel);



    }
}

void TableViewOperation::display(int min,int max){

    DEBUG_TRACE(mPageCount);
    QString query="";
    query="SELECT ROWID,* FROM "+mTable+" WHERE ROWID BETWEEN "+QString::number(min)+" AND "+QString::number(max)+";";
    if(!mSql.exec(query)){

        DEBUG_TRACE(query);
    }
    else{
        DEBUG_TRACE(query);
        DEBUG_TRACE(mSql.size());

        for(int index=min;index<(max);++index)
        {
            mSql.next();
            QStringList columnData;
            for (unsigned int i=0;i<mColumn;i++){
                this->setItem(index,i,new QTableWidgetItem(mSql.value(i).toString()));
            }
        }
    }
    mNoOfRows=mNoOfRows+10000;

}

void TableViewOperation::setRowsToFetch(int row){

    if(!mFilterFlag)
        DEBUG_TRACE(mFilterFlag);

    if(mPageCount!=mNoOfPages+1){
        mPageCount++;
        if(mPageCount<=mNoOfPages){

            display(mNoOfRows,mNoOfRows+10000);
        }
        else
            display(mNoOfRows,mNoOfRows+mRemRecord);
    }
}

void TableViewOperation::setChanContextMenu(const QPoint &pos){

    QMenu *menu = new QMenu(this);

    this->setChannel(this->itemAt(pos)->column());

    auto listChan = menu->addAction("List...");
    listChan->setIcon(QIcon("../IMAGES/list.svg"));
    QSignalMapper* signalMapperDisplay= new QSignalMapper (this) ;
    connect (listChan, SIGNAL(triggered()), signalMapperDisplay, SLOT(map())) ;
    signalMapperDisplay->setMapping(listChan,this) ;
    connect(signalMapperDisplay, &QSignalMapper::mappedWidget ,mpChannel, &ChannelMenuOperation::listChan);

    auto newChan= menu->addAction("New...");
    newChan->setIcon(QIcon("../IMAGES/new-document.svg"));
    QSignalMapper* signalMapperNew = new QSignalMapper (this) ;
    connect (newChan, SIGNAL(triggered()), signalMapperNew, SLOT(map())) ;
    signalMapperNew->setMapping(newChan,this) ;
    connect(signalMapperNew, &QSignalMapper::mappedWidget ,mpChannel, &ChannelMenuOperation::newChan);

    auto delChan= menu->addAction("Delete");
    delChan->setIcon(QIcon("../IMAGES/delete-file.svg"));
    QSignalMapper* signalMapperDel = new QSignalMapper (this) ;
    connect (delChan, SIGNAL(triggered()), signalMapperDel, SLOT(map())) ;
    signalMapperDel->setMapping(delChan,this) ;
    connect(signalMapperDel, &QSignalMapper::mappedWidget ,mpChannel, &ChannelMenuOperation::deleteChan);

    auto copyChan= menu->addAction("Copy");
    copyChan->setIcon(QIcon("../IMAGES/copy.svg"));
    QSignalMapper* signalMapperCopy = new QSignalMapper (this) ;
    connect (copyChan, SIGNAL(triggered()), signalMapperCopy, SLOT(map())) ;
    signalMapperCopy->setMapping(copyChan,this) ;
    connect(signalMapperCopy, &QSignalMapper::mappedWidget ,mpChannel, &ChannelMenuOperation::copyCreate);

    auto hideChan= menu->addAction("Hide");
    QSignalMapper* signalMapperHide = new QSignalMapper (this) ;
    connect (hideChan, SIGNAL(triggered()), signalMapperHide, SLOT(map())) ;
    signalMapperHide->setMapping(hideChan,this) ;
    connect(signalMapperHide, &QSignalMapper::mappedWidget ,mpChannel, &ChannelMenuOperation::hide);

    auto displayAllChan= menu->addAction("Display All");
    QSignalMapper* signalMapperDisplayAll = new QSignalMapper (this) ;
    connect (displayAllChan, SIGNAL(triggered()), signalMapperDisplayAll, SLOT(map())) ;
    signalMapperDisplayAll->setMapping(displayAllChan,this) ;
    connect(signalMapperDisplayAll, &QSignalMapper::mappedWidget ,mpChannel, &ChannelMenuOperation::displayAll);

    menu->exec(QCursor::pos());

}


void TableViewOperation::setChannel(int index){

    if(mSelectedChan!=index){
        mSelectedChan=index;
    }
    DEBUG_TRACE(mSelectedChan);

}

QString& TableViewOperation::getTableName(){

    return mTable;
}


int& TableViewOperation::getSelectedChannel(){

    return mSelectedChan;
}

QStringList& TableViewOperation::getChannelList(){
    return mColumnNames;
}

void TableViewOperation::refreshTable(){
    DEBUG_TRACE(mSelectedChan);
    this->setColumn();
    this->clear();
    this->mPageCount=0;
    this->mNoOfRows=0;
    this->setRowCount(0);
    this->setColumnCount(0);
    this->showData();
}

void TableViewOperation::setChannelToHiddenState(int& channel){
    if(!mHiddenChannels.contains(channel)){
        mHiddenChannels.append(channel);
        if(!this->isColumnHidden(channel))
            this->setColumnHidden(channel,true);
    }
    DEBUG_TRACE(mHiddenChannels);
}

void TableViewOperation::setChannelToDisplayState(const int& channel){
    if(mHiddenChannels.contains(channel)){
        mHiddenChannels.removeOne(channel);
        if(this->isColumnHidden(channel))
            this->setColumnHidden(channel,false);
    }
    DEBUG_TRACE(mHiddenChannels);
}

QList<int>& TableViewOperation::getHiddenChannelList(){

    return mHiddenChannels;
}
