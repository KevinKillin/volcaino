/**
 *  @file   TableViewOperation.h
 *  @brief  Displays Database data in tabular form and applies operations
 *  @author Rachana Kapoor
 *  @date   2021-12-14
 ***********************************************/

#ifndef TABLEVIEWOPERATION_H
#define TABLEVIEWOPERATION_H

#include <QTableWidget>
#include <QMdiArea>
#include <QListWidget>
#include <QSqlQuery>
#include <QPoint>
#include <map>
#include "ChannelMenuOperation.h"
#include "TypeDefConst.h"
class ChannelMenuOperation;
class TableViewOperation:public QTableWidget
{
    Q_OBJECT
public:

    ///default constructor
    TableViewOperation();

    /// Class destructor
    ~TableViewOperation();

    /// @brief static function
    ///
    /// @return   Instance of class TableViewOperation
    ///
    static TableViewOperation* getInstance();

    /// @brief This function sets the class data members when table name is selected.
    /// @return  void
    ///
    void itemClicked(QListWidgetItem *item);

    /// @brief This function shows the table selected in tabular form.
    /// @return  void
    ///
    void showData();

    /// @brief This function sets appends table with more data when mouse scroll is detected.
    /// @return  void
    ///
    void setSliderData();

    /// @brief This function sets the table name.
    /// @return  void
    ///
    void setTableName(QString table);

    /// @brief This function sets the no of rows to fetch.
    /// @return  void
    ///
    void setRow();

    /// @brief This function sets the column names.
    /// @return  void
    ///
    void setColumn();

    /// @brief This function sets the table range to fetch data.
    /// @return  void
    ///
    void setTableRange(int i);

    /// @brief This function adds the another subwindow.
    /// @return  void
    ///
    void setMDIArea(QMdiArea *mdiArea);

    /// @brief This function renames the channel name.
    /// @return  void
    ///
    void renameChannel(int channel);

    /// @brief This function filters the flight and Line data as per flight/light No selected.
    /// @return  void
    ///
    void filterChannel(int channel);

    /// @brief This function display the data as per range set on mouse scroll event.
    /// @return  void
    ///
    void display(int min,int max);

    /// @brief This function sets the no of rows to fetch from database.
    /// @return  void
    ///
    void setRowsToFetch(int row);

    /// @brief This function retrieves the filtered data from database and displays it.
    /// @param item: Item selected from the list
    /// @return  void
    ///
    void setFilter(QListWidgetItem* item);

    /// @brief This function creates a context menu on mouse's right click and invokes the function
    /// depending on the selected menu option.
    /// @param pos: position of the mouse when right click pressed
    /// @return  void
    ///
    void setChanContextMenu(const QPoint &pos);

    /// @brief This function is responsible for getting the valid table name.
    /// @return  table: Name of the table
    ///
    QString& getTableName();

    /// @brief This function is responsible for getting the index of the selected channel.
    /// @return  index: index of the channel
    ///
    int& getSelectedChannel();

    /// @brief This function is responsible for getting the list of the channels in a table.
    /// @return  list: list of the channels
    ///
    QStringList& getChannelList();

    /// @brief This function is responsible for refreshing the table data.
    /// @return  void
    ///
    void refreshTable();

    /// @brief This function is responsible for getting the list of the hidden channels.
    /// @return  list: list of the hidden channels
    ///
    QList<int>& getHiddenChannelList();


public slots:

    /// @brief This function is responsible for setting the selected channel index.
    /// @param index: index of the channel
    /// @return  void
    ///
    void setChannel(int index);

    /// @brief This function is responsible for hiding the selected channel.
    /// @param channel: index of the channel
    /// @return  void
    ///
    void setChannelToHiddenState(int& channel);

    /// @brief This function is responsible for displaying the hidden channel.
    /// @param index: index of the channel
    /// @return  void
    ///
    void setChannelToDisplayState(const int& channel);

private:

    QMdiArea *mMdiArea;

    QSqlQuery mSql;

    unsigned int mRow;

    unsigned int mColumn;

    unsigned int mCount;

    unsigned int mMin;

    unsigned int mMax;

    int mNoOfPages;

    int mRemRecord;

    int mPageCount;

    int mNoOfRows;

    QString mTable;

    QString mFilterColumn;

    QStringList mColumnNames;

    QTableWidget *tableWidget;

    bool mFilterFlag;

    int mSelectedChan;

    ChannelMenuOperation *mpChannel;

    QList<int> mHiddenChannels;

    bool mClick;

};

#endif // TABLEVIEWOPERATION_H
