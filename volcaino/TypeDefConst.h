#ifndef TYPEDEFCONST_H
#define TYPEDEFCONST_H
#include <QDebug>

///@brief DEBUG is defined for the enabling the debug statements.
/// Comment the below line (#define DEBUG) to disable the debug statements
#define DEBUG

#ifdef DEBUG
#define DEBUG_TRACE(log) qDebug()<< __FILE__ << __LINE__ <<"::::" <<__FUNCTION__ << log
#else
#define DEBUG_TRACE(log)
#endif

#endif // TYPEDEFCONST_H
