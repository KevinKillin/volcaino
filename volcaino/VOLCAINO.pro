QT       += core gui
QT       += sql


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ChannelMenuOperation.cpp \
    DataBase.cpp \
    DatabaseTable.cpp \
    ImportFile.cpp \
    TableViewOperation.cpp \
    WindowLayout.cpp \
    main.cpp \
    loginscreen.cpp \
    mainwindow.cpp

HEADERS += \
    ChannelMenuOperation.h \
    DataBase.h \
    DatabaseTable.h \
    ImportFile.h \
    TableViewOperation.h \
    TypeDefConst.h \
    WindowLayout.h \
    loginscreen.h \
    mainwindow.h

FORMS += \
    DatabaseTable.ui \
    loginscreen.ui \
    mainwindow.ui

OTHER_FILES+

CONFIG += lrelease
CONFIG += embed_translations

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc

DISTFILES +=
