#include "loginscreen.h"
#include "ui_loginscreen.h"

LoginScreen::LoginScreen(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::LoginScreen)
{
    ui->setupUi(this);}

LoginScreen::~LoginScreen()
{
    delete ui;
}


void LoginScreen::on_pushButton_login_clicked()
{
    this->close();
    mMainWin.showWindow();
    DEBUG_TRACE(this->height());
}


void LoginScreen::on_pushButton_login_cancel_clicked()
{
  this->close();
}


