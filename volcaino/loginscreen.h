/**
 *  @file   LoginScreen.h
 *  @brief  Login functionality
 *  @author Rachana Kapoor
 *  @date   2021-12-14
 ***********************************************/

#ifndef LOGINSCREEN_H
#define LOGINSCREEN_H
#include "mainwindow.h"
#include <QMainWindow>
#include "TypeDefConst.h"
QT_BEGIN_NAMESPACE
namespace Ui { class LoginScreen; }
QT_END_NAMESPACE

class LoginScreen : public QMainWindow
{
    Q_OBJECT

public:
    ///Class constructor
    LoginScreen(QWidget *parent = nullptr);

    /// Class destructor
    ~LoginScreen();

private slots:

    /// @brief This function checks  username & password of the user and
    /// opens the main window on successful login.
    /// @return  void
    ///
    void on_pushButton_login_clicked();

    /// @brief This function closes the GUI when "CANCEL" button is pressed.
    /// @return  void
    ///
    void on_pushButton_login_cancel_clicked();

private:

    Ui::LoginScreen *ui;

    MainWindow mMainWin;
};
#endif // LOGINSCREEN_H
