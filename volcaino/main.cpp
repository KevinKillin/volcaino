#include "loginscreen.h"
#include "DataBase.h"
#include <QApplication>
#include <QLocale>
#include <QTranslator>


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    DataBase::getInstance()->createDatabase("MainThreadConnection");

    LoginScreen w;

    w.show();

    return app.exec();
}
