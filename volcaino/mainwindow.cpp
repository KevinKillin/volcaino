﻿#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMovie>
#include "DatabaseTable.h"
#include "ImportFile.h"
#include "TypeDefConst.h"


#include <QTableWidget>
#include <QListWidget>
#include <QTextEdit>
#include <QMdiSubWindow>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTreeWidget>
#include <QPushButton>


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    stopped{false}
{

    ui->setupUi(this);
    layout = new WindowLayout;
    mdiArea = new QMdiArea(this);
    mdiArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    mdiArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);


    layout->addWidget(mdiArea, WindowLayout::Center);

    mpSideWin=new QFrame;
    mpSideWin->setFrameStyle(QFrame::Box | QFrame::Raised);
    mpSideWin->setMinimumSize(this->width(),this->height());

    mpSideBar=new QFrame;
    mpSideBar->setFrameStyle(QFrame::Box | QFrame::Raised);
    mpSideBar->setMinimumSize(this->width()/10,this->height());

    mpToolBar=new QFrame;
    mpToolBar->setFrameStyle(QFrame::Box | QFrame::Raised);
    mpToolBar->setMinimumSize(this->width(),this->height()/8);

    DEBUG_TRACE(this->width());
    DEBUG_TRACE(this->height());

    layout->addWidget(mpSideBar, WindowLayout::Left);

    layout->addWidget(mpSideWin, WindowLayout::Left);

    layout->addWidget(mpToolBar, WindowLayout::Top);

    ui->centralwidget->setLayout(layout);
    this->setWindowTitle("VOLCAINO");


    mpTableView=TableViewOperation::getInstance();
    mpTableView->setMDIArea(mdiArea);

    this->createMenubar();

    this->createSidePane();

    this->createToolbar();

    mpDialog = new QDialog(this);
    connect(ImportFile::getInstance(),&ImportFile::started,this,&MainWindow::displayMessage);
    connect(ImportFile::getInstance(),&ImportFile::finished,this,&MainWindow::stopMessage);
    connect(mpDialog,&QDialog::finished,this,&MainWindow::displayMessage);


}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::createMenubar(){
    this->stopped=false;

    mpFileMenu = menuBar()->addMenu(tr("&IMPORT"));


    mpOpenCSV = new QAction(tr("&CSV TYPE"), this);
    mpFileMenu->addAction(mpOpenCSV);

    mpOpenXYZ = new QAction(tr("&XYZ TYPE"), this);
    mpFileMenu->addAction(mpOpenXYZ);

    connect (mpOpenCSV, SIGNAL(triggered()), DatabaseTable::getInstance(), SLOT(selectCSVFile())) ;
    connect (mpOpenXYZ, SIGNAL(triggered()), DatabaseTable::getInstance(), SLOT(selectXYZFile())) ;

    mpExport = new QAction(tr("&EXPORT"), this);
    menuBar()->addAction(mpExport);

    DEBUG_TRACE(mdiArea->height());


}

void MainWindow::displayMessage(){

    if(!stopped){
        mpDialog->setWindowTitle("Alert!!!!!");
        mpDialog->setMaximumHeight(200);
        mpDialog->setMaximumWidth(200);
        mpDialog->setWindowFlags(mpDialog->windowFlags() & ~Qt::WindowContextHelpButtonHint);
        QLabel *processLabel = new QLabel(mpDialog);
        processLabel->setGeometry(0,0,200,200);
        QMovie *movie = new QMovie("C:/Users/rachana.kapoor/Documents/VOLCAINO/IMAGES/pleasewait.gif",QByteArray(),processLabel);
        processLabel->setMovie(movie);
        mpDialog->show();

        movie->start();
    }

}

void MainWindow::stopMessage(){

    DEBUG_TRACE("");
    this->stopped=true;
    mpDialog->close();
}



void MainWindow::createSidePane(){

    QWidget* tab = new QWidget(mpSideWin);

    mpSideDBButton=new QPushButton(mpSideBar);
    mpSideDBButton->setText("<");
    mpSideDBButton->setMaximumSize(30,30);


    QVBoxLayout* tabLayout = new QVBoxLayout;
    tab->setLayout(tabLayout);

    mpSideTab = new QWidget;

    tabLayout->addWidget(mpSideTab);

    QVBoxLayout* widgetLayout = new QVBoxLayout;
    mpSideTab->setLayout(widgetLayout);

    mpListWidget = new QListWidget;

    widgetLayout->addWidget(mpListWidget);
    QStringList list;
    auto db =DataBase::getInstance();
    if(db->openDatabase()){
        list=db->getTableList();
    }

    for(int i = 0; i != list.size(); ++i)
        (new QListWidgetItem(mpListWidget))->setText(list[i]);

    connect (mpListWidget, &QListWidget::itemClicked, this, &MainWindow::itemClicked) ;
    connect (mpSideDBButton, &QPushButton::clicked, this, &MainWindow::setHideUnhide) ;

}

void MainWindow::showWindow(){

    this->showMaximized();
    mpSize=new QSize(mdiArea->width(),mdiArea->height());
}

void MainWindow::itemClicked(QListWidgetItem *item){

    auto *child = createMdiChild();
    child->itemClicked(item);
}

TableViewOperation *MainWindow::createMdiChild()
{
    TableViewOperation *child = new TableViewOperation;
    mdiArea->addSubWindow(child);
    DEBUG_TRACE(child);

   // connect (mpRenameChanButton, &QPushButton::pressed,child , &TableViewOperation::setRenameChannel) ;
   // connect (mpFilterButton, &QPushButton::pressed,child , &TableViewOperation::setFilterChannel) ;

    return child;
}

void MainWindow::setHideUnhide(bool checked){

    if(mpSideTab->isVisible()==true){
        mpSideTab->setVisible(false);
        mpSideWin->setMinimumSize(0,this->height());
        mpSideDBButton->setText(">");
    }
    else{
        mpSideTab->setVisible(true);
        mpSideWin->setMinimumSize(this->width()/6.4,this->height());
        mpSideDBButton->setText("<");

    }

}

void MainWindow::createToolbar(){
   // QHBoxLayout* layout = new QHBoxLayout;
    //mpToolBar->setLayout(layout);
    mpRenameChanButton=new QPushButton(mpToolBar);
   // mpRenameChanButton->setText("RENAME");
    mpRenameChanButton->setMinimumSize(30,30);
    mpRenameChanButton->setIcon(QIcon("../IMAGES/rename.svg"));
    mpRenameChanButton->setIconSize(QSize(30, 30));
    mpRenameChanButton->setStyleSheet("QPushButton{border-radius:5px;border: 1px solid #345781;}") ;
    mpRenameChanButton->move(10,3);

    mpDelChanButton=new QPushButton(mpToolBar);
   // mpRenameChanButton->setText("RENAME");
    mpDelChanButton->setMinimumSize(30,30);
    mpDelChanButton->setIcon(QIcon("../IMAGES/delete-column.svg"));
    mpDelChanButton->setIconSize(QSize(30, 30));
    mpDelChanButton->setStyleSheet("QPushButton{border-radius:5px;border: 1px solid #345781;}") ;
    mpDelChanButton->move(50,3);

    mpFilterButton=new QPushButton(mpToolBar);
   // mpRenameChanButton->setText("RENAME");
    mpFilterButton->setMinimumSize(30,30);
    mpFilterButton->setIcon(QIcon("../IMAGES/filter.svg"));
    mpFilterButton->setIconSize(QSize(30, 30));
    mpFilterButton->setStyleSheet("QPushButton{border-radius:5px;border: 1px solid #345781;}") ;
    mpFilterButton->move(90,3);

   // layout->addWidget(mpRenameChanButton);
   // layout->addWidget(mpDelChanButton);

}

