/**
 *  @file   mainwindow.h
 *  @brief  Mainwindow and Menu Bar related functionality
 *  @author Rachana Kapoor
 *  @date   2021-12-14
 ***********************************************/


#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QProgressDialog>
#include <QMessageBox>
#include <QDebug>
#include <QMdiArea>
#include <QListWidget>
#include "TableViewOperation.h"
#include "WindowLayout.h"
#include "TypeDefConst.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    ///Class constructor
    explicit MainWindow(QWidget *parent = nullptr);

    /// Class destructor
    ~MainWindow();

public slots:
    /// @brief This function is responsible for creating the menu bar and
    /// assigning actions to be taken when pressed.
    /// @return  void
    ///
    void createMenubar();

    /// @brief This function displays the progress wheel if the application
    /// is busy executing a certain task.
    /// @return  void
    ///
    void displayMessage();

    /// @brief This function closes the progress wheel when the ongoing task is completed.
    /// @return  void
    ///
    void stopMessage();

    /// @brief This function shows table data.
    /// @return  void
    ///
    //void showData(QString table);

    /// @brief This function shows table data.
    /// @return  void
    ///
    void createSidePane();

    void showWindow();

    void itemClicked(QListWidgetItem *item);

    TableViewOperation* createMdiChild();

    void setHideUnhide(bool checked);

    /// @brief This function is responsible for creating the tool bar items and
    /// assigning actions to be taken when pressed.
    /// @return  void
    ///
    void createToolbar();


private:

    Ui::MainWindow *ui;

    QMenu *mpFileMenu;

    QMenu *mpCSVMenu;

    QMenu *mpXYZMenu;

    QAction *mpOpenCSV;

    QAction *mpOpenXYZ;

    QAction *mpLineData;

    QDialog* mpDialog;

    bool stopped;

    QMenu *mpShowTableMenu;

    QAction *mpShow;

    QMdiArea *mdiArea;

    QListWidget *mpListWidget;

    QSize *mpSize;

    QAction *mpExport;

    TableViewOperation *mpTableView;

    QFrame *mpSideWin;

    QFrame *mpSideBar;

    QPushButton *mpSideDBButton;

    QWidget* mpSideTab;

    WindowLayout *layout;

    QFrame *mpToolBar;

    QPushButton *mpRenameChanButton;

    QPushButton *mpDelChanButton;

    QPushButton *mpFilterButton;

};

#endif // MAINWINDOW_H
