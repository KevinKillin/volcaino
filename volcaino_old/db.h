#pragma once

#include <iostream>
#include <sqlite3.h>
#include <sstream>
#include <string_view>

namespace database {
	template <typename T>
	class db {
	public:
		static int open_database();
		static int close_database();
		static int create_new_table(std::string_view tableName);
		static int create_projects_table();
		static int general_insert(std::string_view query);
		static int insert_into_dataset_table(int idx, T one, T two, T three, std::string_view tableName);
		static int use_transaction(bool startTransaction);
		static sqlite3* get_db_handle();
	private:
		static inline const char DB_NAME[] = "volcaino.ndb";
		static inline sqlite3* localDB;

		static int callback(void* NotUsed, int argc, char** argv, char** azColName);
	};
	template<typename T>
	inline int db<T>::open_database()
	{
		int rc = SQLITE_OK;

		rc = sqlite3_open(DB_NAME, &localDB);
		if (rc != SQLITE_OK) {
			std::cout << "SQLite Error (in open_database): " << sqlite3_errmsg(localDB) << std::endl;
			sqlite3_close(localDB);
			return rc;
		}

		create_projects_table();

		return rc;
	}
	template<typename T>
	inline int db<T>::close_database()
	{
		int rc = SQLITE_OK;

		rc = sqlite3_close(localDB);
		if (rc != SQLITE_OK) {
			std::cout << "SQLite Error (in close_database): " << sqlite3_errmsg(localDB) << std::endl;
			sqlite3_close(localDB);
			return rc;
		}

		return rc;
	}
	template<typename T>
	inline int db<T>::create_new_table(std::string_view tableName)
	{
		//CREATE TABLE project_data_demo(idx INTEGER, easting REAL, northing REAL, mag REAL);
		int rc = SQLITE_OK;
		char* zErrMsg = 0;

		std::ostringstream out;
		out << "CREATE TABLE IF NOT EXISTS workspace_transforms_" << tableName << "(idx INTEGER, easting REAL, northing REAL, mag REAL);";
		std::string q = out.str();
		const char* qry = q.c_str();
		rc = sqlite3_exec(localDB, qry, NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK) {
			std::cout << "SQLite Error: " << zErrMsg << std::endl;
			sqlite3_free(zErrMsg);
			return rc;
		}

		std::ostringstream out2;
		out2 << "INSERT INTO workspace_transforms_" << tableName << " SELECT * FROM project_data_" << tableName;
		q = out2.str();
		qry = q.c_str();
		rc = sqlite3_exec(localDB, qry, NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK) {
			std::cout << "SQLite Error: " << zErrMsg << std::endl;
			sqlite3_free(zErrMsg);
			return rc;
		}

		return rc;
		
	}
	template<typename T>
	inline int db<T>::create_projects_table()
	{
		int rc = SQLITE_OK;
		char* zErrMsg = 0;

		std::string query = "CREATE TABLE IF NOT EXISTS projects(puid INTEGER PRIMARY KEY, project_name TEXT UNIQUE, creation_date DATE, last_save_date DATE);";
		const char* qry = query.c_str();
		rc = sqlite3_exec(localDB, qry, NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK) {
			std::cout << "SQLite Error: " << zErrMsg << std::endl;
			sqlite3_free(zErrMsg);
			return rc;
		}

		query = "CREATE TABLE IF NOT EXISTS workspaces(wuid INTEGER PRIMARY KEY, puid INTEGER, workspace_name TEXT, creation_date DATE, last_save_date DATE, FOREIGN KEY(puid) REFERENCES projects(puid));";
		const char* qry2 = query.c_str();
		rc = sqlite3_exec(localDB, qry2, NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK) {
			std::cout << "SQLite Error: " << zErrMsg << std::endl;
			sqlite3_free(zErrMsg);
			return rc;
		}

		return rc;
	}
	template<typename T>
	inline int db<T>::callback(void* NotUsed, int argc, char** argv, char** azColName)
	{
		int i;
		for (i = 0; i < argc; i++) {
			printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
		}
		printf("\n");
		return 0;
	}
	template<typename T>
	inline int db<T>::general_insert(std::string_view query)
	{
		int rc = SQLITE_OK;
		char* zErrMsg = 0;

		const char* qry = query.data();
		rc = sqlite3_exec(localDB, qry, NULL, 0, &zErrMsg);
		if (rc != SQLITE_OK) {
			std::cout << "SQLite Error: " << zErrMsg << std::endl;
			sqlite3_free(zErrMsg);
			return rc;
		}

		return rc;
	}
	template<typename T>
	inline int db<T>::insert_into_dataset_table(int idx, T easting, T northing, T magValue, std::string_view tableName)
	{
		//https://stackoverflow.com/questions/13404667/c-sqlite-importing-entire-csv-file-in-c-interface
		std::string qry1 = "insert into ";
		std::ostringstream out;
		out << qry1 << tableName << "(idx, easting, northing, mag) values(" <<
		//out << qry1 << tableName << "(idx, dataset_name, easting, northing, mag) values(" << 
			std::to_string(idx) << "," << 
			//"'" << datasetName << "'," <<
			std::to_string(easting) << "," << 
			std::to_string(northing) << "," << 
			std::to_string(magValue) << ");";
		std::string q = out.str();

		int rc = SQLITE_OK;
		char* zErrMsg = 0;

		const char* qry = q.c_str();
		rc = sqlite3_exec(localDB, qry, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK) {
			std::cout << "SQLite Error: " << zErrMsg << std::endl;
			sqlite3_free(zErrMsg);
			return rc;
		}

		return rc;
	}
	template<typename T>
	inline int db<T>::use_transaction(bool startTransaction)
	{
		int rc = SQLITE_OK;
		char* zErrMsg = 0;

		if (startTransaction == true) {
			std::string query = "BEGIN TRANSACTION;";

			const char* qry = query.c_str();
			rc = sqlite3_exec(localDB, qry, NULL, 0, &zErrMsg);
			if (rc != SQLITE_OK) {
				std::cout << "SQLite Error: " << zErrMsg << std::endl;
				sqlite3_free(zErrMsg);

				return rc;
			}

			return rc;
		}
		else {
			std::string query = "COMMIT;";

			const char* qry = query.c_str();
			rc = sqlite3_exec(localDB, qry, NULL, 0, &zErrMsg);
			if (rc != SQLITE_OK) {
				std::cout << "SQLite Error: " << zErrMsg << std::endl;
				sqlite3_free(zErrMsg);

				return rc;
			}

			return rc;
		}
	}
	template<typename T>
	inline sqlite3* db<T>::get_db_handle()
	{
		return localDB;
	}
}