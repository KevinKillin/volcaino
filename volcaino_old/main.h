#pragma once

typedef double type_t;

const std::string MAJOR = "0";
const std::string MINOR = "0";
const std::string BUILD = "0";
const std::string REV = "0";

void write_gbn_to_db(int count, type_t easting, type_t northing, type_t mag, std::string_view tableName);
