#include "pch.h"
#include "CppUnitTest.h"
#include "../volcaino/grid.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

typedef double type_t;

namespace volcainotest
{
	TEST_CLASS(gridtest)
	{
	public:
		TEST_METHOD(TestDefaultGridSize)
		{
			int defaultGridSize = 0;
			volc::grid<type_t> g;

			Assert::AreEqual(defaultGridSize, g.get_size().first);
			Assert::AreEqual(defaultGridSize, g.get_size().second);
		}
		TEST_METHOD(TestRowColGridSize) 
		{
			int rowSize = 3;
			int colSize = 4;
			volc::grid<type_t> g(rowSize, colSize);

			Assert::AreEqual(rowSize, g.get_size().first);
			Assert::AreEqual(colSize, g.get_size().second);
		}
		TEST_METHOD(TestGridGridSize)
		{
			int rowSize = 3;
			int colSize = 3;
			std::vector<type_t> resRow(colSize, 0);
			volc::grid<type_t>::grid_t result(rowSize, resRow);

			Assert::AreEqual(rowSize, (int)result.size());
			Assert::AreEqual(colSize, (int)result.at(0).size());
		}
		TEST_METHOD(TestClipping)
		{
			int rowSize = 3;
			int colSize = 3;
			type_t initialValue = 20;
			std::vector<type_t> resRow(colSize, 0);
			volc::grid<type_t>::grid_t result(rowSize, resRow);
			
			// Initialize grid to fixed size
			for (int row = 0; row < rowSize; row++) {
				for (int col = 0; col < colSize; col++) {
					result.at(row).at(col) = initialValue;
				}
			}

			// Test 1: Do not clip, result should be grid values unchanged
			volc::grid<type_t> test(result);
			// perform clipping
			type_t min = 0.0;
			type_t max = 200.0;
			volc::grid<type_t> testResult = test.grid_clip(min, max);

			for (int row = 0; row < rowSize; row++) {
				for (int col = 0; col < colSize; col++) {
					Assert::AreEqual(initialValue, testResult.get_grid().at(row).at(col));
				}
			}

			// Test 2: intentionally clip, result should be grid values clipped to max
			min = 0.0;
			max = 1.0;
			testResult = test.grid_clip(min, max);

			for (int row = 0; row < rowSize; row++) {
				for (int col = 0; col < colSize; col++) {
					Assert::AreEqual(max, testResult.get_grid().at(row).at(col));
				}
			}
		}
	};
}
